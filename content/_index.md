+++
title = "Hadi Lashkari Ghouchani"
description = "A professional Android developer and an amateur Theoretical Physicist."
+++


Hey there! I am Hadi!

I'm the turtle in "the turtle and the rabbit" story,
so you will find me while I keep pushing forward to think deeper about the universe all the time.
Sometimes I cannot let a problem go out of my mind, like more than a decade, if it's interesting enough,
however, as the story has been prophesied in the end the turtle's approach is getting me faster than others to the goals.
This is my scientist hat.

Nevertheless, I have an engineer hat too,
which let me think like an engineer to get the production out in the customer's hand,
as fast as Neovim can get!
I like to make stable, scalable, maintainable, testable, robust, and correct apps.

You can always find my updated resume in [GitHub](https://github.com/hadilq/cv-english/blob/main/cv-public-lashkari-mobile.pdf).

🚀 Open to New Opportunities After 19+ Years in Software Development!

After concluding my role at Bell Canada, I'm seeking new challenges where I can leverage my extensive experience across software development domains.

💡 What I bring:

    - 19+ years of software development expertise, including 9 years specialized in Android
    - Deep understanding of software architecture and algorithm design
    - Experience applying ML to real-world problems
    - Track record of building scalable, maintainable systems
    - Former university lecturer in algorithms and programming

🔧 Technical Philosophy:

    - Advocate for SOLID principles, SSOT, and functional programming
    - Focus on building resilient, reproducible systems
    - Emphasis on compile-time safety (like Rust) over runtime checks
    - Commitment to transparent technical decision-making

🎯 Ideal Opportunities:

    - Complex technical challenges requiring architectural expertise
    - Roles involving Android, backend, or ML engineering
    - Teams valuing software craftsmanship and engineering excellence
    - Organizations building scalable, mission-critical systems

Currently open to roles where I can apply my comprehensive Android ecosystem knowledge and broad technical expertise to create impactful solutions.
Let's connect!


I'm mostly active in Twitter(X) these days in case you want to contact me publicly.



