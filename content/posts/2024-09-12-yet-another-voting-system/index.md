+++
title = "Yet another voting system"
date = 2024-09-12
author = "Hadi Lashkari Ghouchani"
summary = "Voting system to breaking two-party system"
draft = false

[taxonomies]
categories = ["Math", "Society"]
tags = [
 "Math",
 "Society",
]
+++

![Yet another voting system](wallpaper.webp)

Do you like to have more diverse parties in elections? Let's think about it.

<!-- more -->
-------------

Above wallpaper reference[^wallpaper]

It's my extension to the Vertasium's video

{{ youtube(id="qf7ws2DF-zk") }}


Here is my effort with the same sprite to solve a problem mathematically without engaging in politics.
In the video, to highlight the problem with the _First past the post elections_(FPTP)[^first-past],
Prof. Eric Maskin explains a historical fact about 2000 US presidential election.
Most of voters voted for a candidate out of the main two parties[^two-party],
yet still one of those two parties won the election,
which means the _First past the post voting system_(FPTP)[^first-past] is forcing the two-party system[^two-party].
He explains what _Spoiler effect_[^spoiler-effect] is,
and how FPTP always leads to two-party system[^two-party] by _Duverger's law_[^d-law],
then he concluded that FPTP isn't a great option.
The rest of the video is about the other kinds of election systems,
but because FPTP is a very simple method, which should be the target of any social construct,
the goal here is to change it with a small tweak,
to make it possible to break the two-party system[^two-party] in a circumstance that voters need that.

Notice, the two-party system is not necessarily a bad practice.
It's indeed very productive when you what unity in the society.
But the problem is that there's no way in FPTP to break the loop among two main parties.
Therefore, the only evolution possible is to change people inside those two main parties
by replacing them one by one.
It's happening all the time, but those replacements are not what people directly voted for.
Worse than that, in the author's opinion,
is that those replacements are always breaking the principles, and promises, of any party,
which always leads to paradoxical behavior, and finally transferring into pains for all of us.

Hope the problem is clear by now!
The possible solution to this problem could be the _Split first past the post system_(SFPTP),
where it's explained below.

# Split first past the post system
In the _First past the post voting system_(FPTP),
we can think of each of voted ballots as a \\(n\\) dimensional vector,
where each candidate has an index and only the element on the index of voted candidate is one,
and the rest of elements are zero.
So if you voted for candidate with index \\(3\\), your voted ballot will be

\\[
b = \begin{bmatrix}
0 \\\\
0 \\\\
1 \\\\
0 \\\\
. \\\\
. \\\\
. \\\\
0
\end{bmatrix}
\\]

Hence, the element \\(i\\) in the \\(b\\) that's a voted ballot to \\(c\\),
where \\(c\\) is the index of the voted candidate,
is \\(\delta_{ic}\\), where \\(\delta\\) is Kronecker delta[^k-delta].
Therefore, the total votes would be

\\[
T = \sum_v b_v.
\\]

Where \\(b_v\\) is the voted ballot of a voter with index \\(v\\).
Thus, \\(argmax(T)\\)[^arg-max] is the index of the winner of the election.

Assuming you know how the FPTP[^first-past] works,
it's easy to make sense out of above statements,
which makes us prepared to explain the _Split first past the post system_(SFPTP) precisely.
In this proposed system we ask the voters two questions:

- Who do you vote for if you want to show your unity? : [Answer would be one of the candidates or empty]
- Relax! Don't afraid of anything! Whose values are mostly aligned with your value? : [Answer would be one of the candidates or empty]

Also we mention in the ballot that:
- You can keep the answer empty,
- You cannot fill both of them with the same name, otherwise, the ballot will be read as empty.

These two questions will give us two vectors, \\(b_v\\) and \\(r_v\\) of one voter, with index \\(v\\).
Of course, the empty answer is \\(0\\) vector, and our condition is \\(b_v \neq r_v\\),
which MUST hold.
Then again to calculate the total votes we follow a very similar process as FPTP
above.

\\[
T = \sum_v b_v + r_v
\\]

And also the winner of SFPTP is \\(argmax(T)\\)[^arg-max].

# What did we change?
In the _Split first past the post system_(SFPTP), that explained above,
we can explore the same scenario in FPTP that parties are pushing for unity by any means,
where voters know if they give up their unity the main opponent party would win,
and _Spoiler effect_ will occur.
In this scenario the FPTP would give up and one of the two main parties will win.
However, in the SFPTP,
you want to show your unity to avoid wining of the main opponent party,
so you still vote for your candidate in one of the main parties,
nonetheless, unlike the FPTP, here your main opponent party cannot win,
because they only can spend half of their votes for their party,
due to \\(b_i \neq r_i\\) condition.
This makes you free of restricting your mind to the unity of your main party,
and think twice which candidate actually holds the same values as your values,
and fill the second choice of the ballot with that candidate.

This will diversify the candidates, but if there is a popular candidate,
out of the two main parties, then voters can choose him/her,
without risking losing to the main opponent party.
This gives us a way to move out of two-party system[^two-party], risk free.

Regarding other possible scenarios,
I cannot see any difference between FPTP,
and SFPTP, however, feel free to explore them.
Therefore, my proof that SFPTP will break _Duverger's law_[^d-law] is finishing here.

# Modifications
There could be some modifications to this system. Let's explore them briefly.
The first option is to split the ballots to more than two choices.
Let's agree that we need unity at some points,
and that's why I am against splitting the votes more,
because if you have noticed the two questions in the ballot in SFPTP are binary deep down,
which means there's no third option.
This brings us to the second option to modify this system by giving non-equal weights to those choices.
Something like

\\[
T = \sum_v w_b \times b_v + w_r \times r_v
\\]

Where \\(w_b \neq w_r\\).
However, it's not a good idea simply because again!
Those two questions are binary, so they should weight equally.
Not my best argument about something,
but it's my option.




# Conclusion
Mathematics can solve your real life problems if you give it a chance by asking the right questions.




-----


# References

[^wallpaper]: [Wikipedia](https://en.wikipedia.org/wiki/File:A_coloured_voting_box_(no_bg).svg)

[^first-past]: [First past the post voting](https://en.wikipedia.org/wiki/First-past-the-post_voting)

[^two-party]: [Two party system](https://en.wikipedia.org/wiki/Two-party_system)

[^spoiler-effect]: [Spoiler effect](https://en.wikipedia.org/wiki/Spoiler_effect)

[^d-law]: [Duverger's law](https://en.wikipedia.org/wiki/Duverger%27s_law)

[^k-delta]: [Kronecker delta](https://en.wikipedia.org/wiki/Kronecker_delta)

[^arg-max]: [Arg max](https://en.wikipedia.org/wiki/Arg_max)

# Cite
If you found this work useful, please consider citing:
```
@misc{hadilq2024Vote,
    author = {{Hadi Lashkari Ghouchani}},
    note = {Published electronically at \url{https://hadilq.com/posts/yet-another-voting-system/}},
    gitlab = {Gitlab source at \href{https://gitlab.com/hadilq/hadilq.gitlab.io/-/blob/main/content/posts/2024-09-12-yet-another-voting-system/index.md}},
    title = {Yet another voting system},
    year={2024},
}
```

