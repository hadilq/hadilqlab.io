+++
title = "Intocave vs. Exocave"
date = 2024-02-09
author = "Hadi Lashkari Ghouchani"
summary = "Review a psychology's bias"
draft = false

[taxonomies]
categories = ["Psychology"]
tags = [
 "Psychology",
 "introvert",
 "extrovert",
 "intocave",
 "exocave",
]
+++

![Pale Blue Dot](Pale_Blue_Dot.png)

I am neither a psychologist nor an economist, but did you notice this problem?

<!-- more -->
-------------

Above wallpaper reference[^wallpaper]

I am interested in psychology, and economy,
so no guarantee what you are about to read makes sense,
but it's not stopping me to write about this problem,
because it has a deep impact on our lives.

You probably heard of introvert personality type vs extrovert one?
Let's read what Wiki [^extro-intro] said:

> The trait of extraversion (also spelled extroversion)
> and introversion are a central dimension in some human personality theories.
> The terms introversion and extraversion were introduced into psychology by Carl Jung,
> though both the popular understanding and current psychological usage are not the same
> as Jung's original concept. Extraversion tends to be manifested in outgoing,
> talkative, energetic behavior, whereas introversion is manifested in more reflective
> and reserved behavior. Jung defined introversion as an "attitude-type characterised by
> orientation in life through subjective psychic contents",
> and extraversion as "an attitude-type characterised by concentration of interest on the external object".

First thing is that you should be aware of a debate that there is no introvert,
or extrovert, personality type [^personality].
It's all about traits as you can read about in the above paragraph.
Any person can have both kinds of traits,
where there's no measure to which kind of traits are more used by a person,
so there's no way to say someone is an introvert, or extrovert.
These traits are teachable, so it makes more sense to talk about introversion, and extroversion, skills.
Also, someone can argue that some of these skills are easier for some people to learn,
therefore, we can separate people in this way, and give them personality type.
However, the counterargument is both of these kinds of traits have no end,
where the learning curve can be different for different people in the beginning,
but how much you learn more the steep of the learning curve increases for everyone.
Hence, nobody can conclude it's a measure.
People are good or bad with these skills because of a lot of parameters,
including

 - What have been taught to them?
 - How much time they spend to learn those skills?
 - Did they have in-sync, or out-of-sync, teaching pressure respect to their learning potentials?
 - etc.

There are better arguments out there about it, so I have nothing more to add here.
Just remember it's all about traits, that we can learn. Let's move on.

Second, let's go back in time! The time when we lived in caves.
Someone can separate the behavior of the people in the cave
by looking if they are doing it inside the cave or outside the cave.
For instance, going out and hunting was happened outside the cave,
where planing a hunt, or celebrating it, or manipulating others to who should go out,
was happened inside the cave.
In this example the cave can clearly definable,
therefore you clearly can separate the activities inside and outside.
However, engaging in activities outside was more risky.
You could be killed by outside animals,
while people who stayed inside the cave would enjoy the security provided by what I call insurance principle.

> The insurance principle is referring to what's happening when a group of people are getting together
> and decrease the risk of an activity respect to when each of them separately wanted to do that.

The risk of being dead when you are outside the cave acted like a selection process.
Not sure if we can call it natural selection, though.
However, it's part of our evolution.
Therefore, with more probability we are the descendant of the people who engaged in inside activities.
Thus, we have a huge bias to do and praise the inside the cave activities. But, are we?

# The cave
Starting from the naming of introversion and extroversion,
can you see any problem with this naming?
It was invisible to me, and it's most likely invisible to you too.
Jung, who coined the introversion, and extroversion,
named them like extroversion traits is a bigger set of behaviors!
Because outside is always bigger than inside, if you noticed.
For instance, in below venn diagram you can see the outside is a bigger set!
The outside could be even bigger, but I wanted to avoid zooming out more!

![in-vs-out](in-vs-out.png)

But Jung and most of the psychologist, and most of people who used this terminology,
had no problem with this! Because we all have bias to see the inside the society bigger!
This bias switched the usages of in and out in the naming of those kinds of traits.
I would call this bias the **big society bias**, or **big cave bias**,
because I cannot find this bias in the cognitive biases list of wiki [^cognetive-bias-list].
Let me know if it already has a name.

Before providing more evidence that we biased toward inside,
I need to change those names forever!
To do so, I am going to assume we have a graph named **cave**,
where its nodes are people,
and the edges are the information transformed among them.
Any trait that involves exchanging information in the cave is named **intocave traits**.
Any other trait that a person can behave on, is named **exocave traits**.
Notice, introversion is close to exocave, and extroversion is close to intocave.

I'm going to use "cave" interchangeably with "society" from now on!
You may ask: why did you name it cave among all the other names available?
Because, it's the closest one to the thought experiment we had above.
The cave is embedded the evolutionary history of what has happened.
Also, if you noticed the wallpaper of this post is the famous picture that's taken by Voyager 1 [^voyager-1].
Taking this picture is famously suggested by Carl Sagan [^carl-sagan].
In this picture you can see that I still have bias to call that graph, a cave!
I should have named it a "dust", "Pale Blue Dot" or "easily ignorable"!
But please forgive me!

Now let's go back to our business to prove that we have the big cave bias.
The bias toward seeing inside the cave bigger than its outside.
I can give you at least two more evidences.
First, just take a look at what kinds of traits will make more money for you.
Yes! intocave traits.
You may know that money is a social tool. A cave tool?
The usage of this tool is to assign value to goods.
But, shouldn't we come up with a way to give at least equal credit to exocave traits as well,
after all these centuries that we invented the economy?
We don't even noticed that we biased toward inside the cave,
even that it bothers us!
We blame people, we blame capitalism, we blame socialism,
but we don't come across the possibility that maybe there is a bias in our behavior that caused these problems.
And that's why it's a bias, because it's invisible to us, unless you try to be self-aware.
We're like a fish who cannot see the ocean.
Regarding the equal credit, should I mention the Edison, and Tesla's story,
while they both had very close talents,
but one of them was more skilled in intocave traits?

My second point to prove that the big cave bias exists is "Socially Interactive Exercise Improves Longevity: The Power of Playing with Friends"
study [^longevity], and similar studies.
**We live longer if we have intocave skills.**
We need intocave traits to be happy, and healthy.
Of course, inside the cave is bigger than the whole outside for it.

I can continue by providing more cases,
but it's enough.
Let's ask better questions, like so what?!
We're people who a century ago was solving our conflicts by killing each others.
Nowadays, in civil countries we resolve all of our conflicts by using these intocave skills.
This is our civilization.
This is an accomplishment, not a problem!
intocave skills, like communication via human languages are much more important than
communication by the nature's language, e,g. having math skills.
Therefore, we have a selection process for people, who want to come to the civil countries,
to evaluate their language skills, not their math skills.
We're great!!

Additionally, someone can argue the whole economy is about selling skills,
and he/she is not wrong.
The marketing skills are how we run the economy,
because it's all about money,
and as mentioned before, money is a cave tool,
so it's natural to have demand for intocave skills all over the place.

# Problem
The problem arises very naturally,
because when you have a boundary the resources inside the boundary are limited.
This restriction will cause conflicts,
where conflicts need to be resolved.
As mentioned before we, as a civilization, achieved this level of intocave skills to avoid killing each others on such conflicts.
However, I can argue we lost the balance on the other side!

Nowadays, we have especial people with highly intocave skills, trained especially to resolve conflicts.
We call them lawyers.
They are our warriers in the modern battles.
Whenever someone doesn't have the sufficient intocave skills to fight in conflicts,
they can hire a lawyer to be the night.
Whenever we need someone to defend our rights in the law making processes,
we vote for someone with highly skilled intocave traits.

It's awesome, until you notice you need lawyers to deal with the governments!!
Nobody has any conflict with the government when he/she only want to do a legal act.
What's the conflict in such a case? Inefficiency?
Indeed, in my humble opinion,
hiring a lawyer to do a legal interaction with the government should be illegal!

But this one was an easy problem to suggest a solution.
I am lucky that the night I started to writing this post, Derek(Veritasium) released below video.
Let's watch it first, so I'll write about the problem afterward.

{{ youtube(id="AF8d72mA41M") }}

Well done Derek! You taught a course in an entertaining video.

In this video, Nakamura is a person with highly exocave skills,
where in the conflicts his lack of intocave skills is obvious.
The result is that an 8 billion dollar industry cannot pay off its debt to one of its greatest inventors.

In my argument above it's not clear what's the relation between intocave traits,
the conflicts, and resources.
And this is where things get interested.

People inside the cave have limited amount of resources,
therefore, if a person with highly intocave skills want to gain more resources,
he/she would seek the conflicts,
because they know that they would win.
**Highly intocave skills totally include the manipulation of the mind of the other side,
and the possible judges.**
It's much more civilized than being a bully, but the idea is the same.
You start the conflicts because you know you'll win.
And the cave will pay you back well. Hi Oracle Corp!

Notice, having highly intocave skills is necessary to sell stuff in the market better,
therefore, we teach people all these intocave skills for good.
But look at the results!

On the other hand, people who can go outside the cave,
of course with their exocave skills,
have unlimited amount of resources.
They just need to figure out how to harness those resources.
They are the actual warriors, because they take a huge risk by being outside the cave.
How many Nakamura out there couldn't dedicate 30 years of their life to a problem because of their bosses?
But look at the generated value that was outside, before Nakamura took it inside.
And it's a blocker for other people who want to develop their exocave skills,
when they can see even such a big industry couldn't pay back to Nakamura,
and similar people.

Thus, people with highly exocave skills don't feel the need to start any conflicts.
They see the resources inside the cave like they are looking at a Pale Blue Dot.
For instance, just look at the attitude of Nakamura in the video above.
**Maybe next step to become more civilized,
after avoid killing each others in the conflicts,
is to decrease the number of conflicts,
by training our people with more exocave skills.**

This problem is more concerning when I look at the cartoons we make for kids these days.
Not sure, but it seems we're not in the right direction.

This post is not about providing a solution to this problem,
but I'm being me, cannot write about an issue
without trying to propose something.

Any solution should involve the decrease of the intocave skills demand.
Here, I would say probably developing a network of trust would help a lot to decrease that demand,
due to the fact that when buyer and seller built trust,
they don't need to rely on manipulation of the other side's mind to sell anymore.
I have a kind of big solution, but I would postpone it until a future post.
However, in the direction of developing only trust, not its network yet,
I can raise a simple idea here.
It'll not fix the whole demand problem,
but it'll help to some extent,
taking into account it's a fix on current infrastructure we already have.
I mean no new infrastructure is needed, probably!

I can explain the idea by bringing the reviews after purchasing a product into your attention.
There are review systems all over the Internet these days.
For instance, the Amazon has a great review system.
They help future buyers to be aware of the features and problems of a product.
They build the trust, even though we cannot call it a network.

Their big problem is fake reviews, of course.
By the way, the fix is relatively easy.
We need to make it costly to review to stop fake reviews.
Another problem is that people tend to give positive reviews more often,
so our solution must make positive reviews more costly than negative ones.

Before going further, let's assume each transaction of a purchase has an ID,
that we call it transaction ID.
What if for any purchase in the economy, we could have a channel ID instead.
So after posting the initial transaction into the channel,
later we could post a review message, and other metadata e.g. ratings, to this channel.
The most important part is that you could attach a payment to that review.
This means buyers after ten years could come back and give a good review along
with an amount of money they think the seller/producer deserves.
Or review it, along with its payment, in monthly basis, because it worked to that date as expected.

The amount of money that's attached to the review must only depend on the buyer's will.
No obligation is attached, only being a trustworthy buyer to pay that amount is the goal.

Therefore, each purchase channel, with unique channel ID,
could have multiple transaction IDs, for different reviews, in the future.
Indeed, currently we can have at least one more transaction ID for our previous purchases,
which could be the refund transaction.
But here the concept is to extend it to accept more positive or negative transactions.
This is why I am saying it's relatively easy because we do it to some extent right now.

So what?!
If the seller trusts the buyers, statistically at least,
that they will pay back if the product works for them,
then the selling prices would decrease,
while the quality increases,
because with higher quality more buyers, statistically,
would come back and give good reviews along with a payment.
Meanwhile, the seller can decrease the prices,
because they now the profit is in the review payments.
Especially, if the governments help to building trust in the society by making the review transactions tax-free,
similar to the refund transactions we currently have.

The review transactions must be transparent to the future buyers,
so they could judge if the review is fake or not.
They can statistically analyze how many of the reviews have transactions,
so filter them out and decide based on them.

If it's hard for you to imagine this model,
think of it like a better way to tipping.
You go to a restaurant and eat,
then pay the fee, however, if you are happy with the service,
you can latter pay the tip into the same channel of your initial payment,
along with a possible rating, and message.
This approach also addresses the recent cultural issue we developed in the society,
because with this method you will not be culturally obligated to pay the tip on site,
in front of your friends to show off!

The corner cases, like
what if the seller is not the producer of the good,
could be handled in different ways, which are open to be discussed further.
They could be more definitions and implementations on how channels can funnel the money to other channels,
where the producer could ask for a percentage of the review transactions to be funneled to their channels.
However, even with such approach the producers would like to sell the product directly to the buyers,
to avoid paying a margin to the retailer.
Therefore, we could ask: do we need retails in the first place?
What's their added value to the society?
There are probably some, so be creative and add up to this idea.

# Conclusion
We need to bring some kind of balance between intocave and exocave skills' value in the society.
To do so we probably need to build our network of trust, more officially.










-----


# References

[^wallpaper]: [Pale Blue Dot](https://en.wikipedia.org/wiki/Pale_Blue_Dot)

[^extro-intro]: [Extraversion and introversion](https://en.wikipedia.org/wiki/Extraversion\_and\_introversion)

[^personality]: [Personality psychology](https://en.wikipedia.org/wiki/Personality\_psychology)

[^cognetive-bias-list]: [List of cognitive biases](https://en.wikipedia.org/wiki/List_of_cognitive_biases)

[^voyager-1]: [Voyager 1](https://en.wikipedia.org/wiki/Voyager_1)

[^carl-sagan]: [Carl Sagan](https://en.wikipedia.org/wiki/Carl_Sagan)

[^longevity]: [Socially Interactive Exercise Improves Longevity: The Power of Playing with Friends](https://www.researchgate.net/publication/326234013_Socially_Interactive_Exercise_Improves_Longevity_The_Power_of_Playing_with_Friends)

# Cite
If you found this work useful, please consider citing:
```
@misc{hadilq2024IntocaveExecave,
    author = {{Hadi Lashkari Ghouchani}},
    note = {Published electronically at \url{https://hadilq.com/posts/intocave-exocave/}},
    gitlab = {Gitlab source at \href{https://gitlab.com/hadilq/hadilq.gitlab.io/-/blob/main/content/posts/2024-02-09-intocave-exocave/index.md}},
    title = {Intocave vs. Exocave},
    year={2024},
}
```

