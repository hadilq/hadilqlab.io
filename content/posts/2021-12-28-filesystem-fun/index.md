+++
title = "Filesystem fun"
date = 2021-12-28
author = "Hadi Lashkari Ghouchani"
summary = "For an unknown reason I got interested on how efficient and scalable we can put the data in a disk!"
draft = true

[taxonomies]
categories = ["Computer Science", "File System"]
tags = [
 "File System",
 "Scalability",
 "Fun",
]
+++

For an unknown reason I got interested on how efficient and scalable we can put the data in a disk!

<!-- more -->

I'll add the details later, but here how I've spend my fun time by designing a lowest layer of a file system.


```
Disk clusters

On 2025, 2^70 byte will be generated everyday!

1 byte = 2^3 bit
i32 = 2^5 bit =0..2^32=4 byte 
i40 = 5 byte
I48 = 6 byte
I56 = 7 byte
i64 = 2^6 bit =0..2^64=8 byte

************* Version 0.1 ***************
1 block = 2^8 byte = 256 byte < 512 bytes

1 cluster = 2^32 block = 2^(2+8) * 2^10 * 2^10 * 2^10 byte = 1 terabyte
Index of blocks on each cluster= 0..2^32-1=32 bit
Repetition of each block=1..2^8=8 bit
Max block with its repetition=2^8*2^8=2^16 byte = 64KB > 16KB
Compact index=(repetition-1)*2^33+index=0..2^40-1=40 bit

1 supercluster = 2^40 cluster = 2^40*2^40 byte = 2^80 byte
Index of clusters on each supercluster=0..2^40-1=0..2^40-1=40 bit
Index of superclusters=0..2^40-1=0..2^40-1=40 bit

Max-size=2^40 supercluster = 2^120 byte almost 10^36 byte
Compact index of each block in disk=120 bit

root=[i40, block-to-byte*byte-to-bit/40]=[i40, 2^8*2^3/40]=[i40, 51]

************* Version 0.2 ***************

1 block = 2^b byte
1 cluster = 2^c block
Repetition of each block in a cluster=1..2^r=r bit
1 supercluster = 2^s cluster
Max-size=2^m supercluster = 2^(b+c+s+m) byte

root=[I(b+r+c+s+m), 8b/(b+c+s+m)]

************* Version 0.3 ***************

Block =  [byte-type, 2^8 byte=256 byte]

untyped-block = [byte-type, 2^8 byte]
root-permission = 000000….00000

// Array of addresses in current page
Block-type=0000=>[byte-type, 2^8 byte] = 
    [block-type, 4 bit] + 
    [hash-of-this-block-except-this-byte-type=i12, 1.5] + 
    [permission-group-type=i64, 8 byte] + 
    [create-time-type=i48, 6] +
    [access-time-type=i48, 6] +
    [modify-time-type=i48, 6] + 
    [next-block-address-type=i8, 1] +
    [address-in-current-page-type=i8, 2^8-29 = 227]

// Array of addresses in current book
Block-type=0001=>[byte-type, 2^8 byte] = 
    [block-type, 4 bit] + 
    [hash-of-this-block-except-this-byte-type=i12, 1.5] + 
    [permission-group-type=i64, 8 byte] + 
    [create-time-type=i48, 6] +
    [access-time-type=i48, 6] +
    [modify-time-type=i48, 6] +
    [next-block-address-type=i16, 2] +
    [address-in-current-book-type=i16, [(2^8-30)/2]]

// Array of addresses in current shelve
Block-type=0010=>[byte-type, 2^8 byte] = 
    [block-type, 4 bit] + 
    [hash-of-this-block-except-this-byte-type=i12, 1.5] + 
    [permission-group-type=i64, 8 byte] + 
    [create-time-type=i48, 6] +
    [access-time-type=i48, 6] +
    [modify-time-type=i48, 6] +
    [next-block-address-type=i24, 3] +
    [address-in-current-shelve-type=i24, [(2^8-31)/3]]

// Array of addresses in current cluster
Block-type=0011=>[byte-type, 2^8 byte] = 
    [block-type, 4 bit] + 
    [hash-of-this-block-except-this-byte-type=i12, 1.5] + 
    [permission-group-type=i64, 8 byte] + 
    [create-time-type=i48, 6] +
    [access-time-type=i48, 6] +
    [modify-time-type=i48, 6] +
    [next-block-address-type=i32, 4] + 
    [address-in-current-cluster-type=i32, [(2^8-32)/4] = 41]


// Array of addresses in current supercluster
Block-type=0100=>[byte-type, 2^8 byte] = 
    [block-type, 4 bit] + 
    [hash-of-this-block-except-this-byte-type=i12, 1.5] + 
    [permission-group-type=i64, 8 byte] + 
    [create-time-type=i48, 6] +
    [access-time-type=i48, 6] +
    [modify-time-type=i48, 6] +
    [next-block-address-type=i40, 5] + 
    [address-in-current-supercluster-type=i40, [(2^8-33)/5]]

// Array of addresses in current library
Block-type=0101=>[byte-type, 2^8 byte] = 
    [block-type, 4 bit] + 
    [hash-of-this-block-except-this-byte-type=i12, 1.5] + 
    [permission-group-type=i64, 8 byte] + 
    [create-time-type=i48, 6] +
    [access-time-type=i48, 6] +
    [modify-time-type=i48, 6] +
    [next-block-address-type=i48, 6] + 
    [address-in-current-library-type=i48, [(2^8-34)/6]]

// Array of addresses in librarycluster
Block-type=0110=>[byte-type, 2^8 byte] = 
    [block-type, 4 bit] + 
    [hash-of-this-block-except-this-byte-type=i12, 1.5] + 
    [permission-group-type=i64, 8 byte] + 
    [create-time-type=i48, 6] +
    [access-time-type=i48, 6] +
    [modify-time-type=i48, 6] +
    [next-block-address-type=i56, 7] + 
    [address-in-current-librarycluster-type=i56, [(2^8-35)/7]]

// Array of addresses in internet
Block-type=0111=>[byte-type, 2^8 byte] = 
    [block-type, 4 bit] + 
    [hash-of-this-block-except-this-byte-type=i12, 1.5] + 
    [permission-group-type=i64, 8 byte] + 
    [create-time-type=i48, 6] +
    [access-time-type=i48, 6] +
    [modify-time-type=i48, 6] +
    [next-block-address-type=i64, 8] + 
    [address-in-current-internet-type=i64, [(2^8-36)/8] = 27]


// Head of a batch of data with meta data
Block-type=1000=>[byte-type, 2^8 byte] =
    [block-type, 4 bit] + 
    [hash-of-this-block-except-this-byte-type=i12, 1.5] + 
    [permission-group-type=i64, 8 byte] +
    [create-time-type=i48, 6] +
    [access-time-type=i48, 6] +
    [modify-time-type=i48, 6] +
    [repetition-of-untyped-blocks-in-current-page-type=i8, 1 byte] +
    [next-block-after-repetition-type=i64, 8 byte] +
    [block-data-value-type, 2^8 - 39 byte]

// Head of a batch of data
Block-type=1001=>[byte-type, 2^8 byte] =
    [block-type, 4 bit] + 
    [hash-of-this-block-except-this-byte-type=i12, 1.5] + 
    [repetition-of-untyped-blocks-in-current-page-type=i8, 1 byte] +
    [next-block-after-repetition-type=i64, 8 byte] +
    [block-data-value-type, 2^8 - 11 byte]

Page = [Block, 2^8] = 2^8*2^8 byte= 2^16 byte = 6K byte
Book = [Page, 2^8] = 2^8*2^8*2^8 byte= 2^24 byte = 16M byte
Shelve = [Book, 2^8] = 2^8*2^8*2^8*2^8 byte= 2^32 byte = 4G byte
Cluster = [Shelve, 2^8] = 2^8*2^8*2^8*2^8*2^8 byte= 2^40 byte = 1T byte
Supercluster = [Cluster, 2^8] = 2^8*2^8*2^8*2^8*2^8*2^8 byte= 2^48 byte = 256T byte
Library = [Supercluster, 2^8] = 2^8*2^8*2^8*2^8*2^8*2^8*2^8*2^8 byte= 2^56 byte = 32P byte
Librarycluster = [Library, 2^8] = 2^8*2^8*2^8*2^8*2^8*2^8*2^8*2^8*2^8 byte= 2^64 byte = 16E byte
Internet = [Librarycluster, 2^8] = 2^8*2^8*2^8*2^8*2^8*2^8*2^8*2^8*2^8*2^8 byte= 2^72 byte = 4Z byte


// Layers
- read/write to primitive types defined above
- encryption
- hash
- keep consistency by finding which bock should be read/write and maintenance the empty spaces
- read/write to composite types defined by users


************* Version 0.4 ***************

// Layers
- read/write to primitive types defined above
- encryption
- hash
- keep consistency by finding which bock should be read/write and maintenance the empty spaces
- read/write to composite types defined by users

byte =  [bit-type, 8 bit]
block =  [byte-type, 2^8 byte=256 byte]
page = [block-type, 2^8] = 2^8*2^8 byte= 2^16 byte = 6K byte
book = [page-type, 2^8] = 2^8*2^8*2^8 byte= 2^24 byte = 16M byte
shelve = [book-type, 2^8] = 2^8*2^8*2^8*2^8 byte= 2^32 byte = 4G byte
sector = [shelve-type, 2^8] = 2^8*2^8*2^8*2^8*2^8 byte= 2^40 byte = 1T byte
cluster = [sector-type, 2^8] = 2^8*2^8*2^8*2^8*2^8*2^8 byte= 2^48 byte = 256T byte
library = [cluster-type, 2^8] = 2^8*2^8*2^8*2^8*2^8*2^8*2^8*2^8 byte= 2^56 byte = 32P byte
ethernet = [library-type, 2^8] = 2^8*2^8*2^8*2^8*2^8*2^8*2^8*2^8*2^8 byte= 2^64 byte = 16E byte
internet = [ethernet-type, 2^8] = 2^8*2^8*2^8*2^8*2^8*2^8*2^8*2^8*2^8*2^8 byte= 2^72 byte = 4Z byte

// Head of array of addresses in current page
Block-type=000000=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [metadata-address-type=i8, 1] +
    [next-block-address-type=i8, 1] +
    [address-in-current-page-type=i8, 2^8 - 4 ]

// Array of addresses in current page
Block-type=000001=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [next-block-address-type=i8, 1] +
    [address-in-current-page-type=i8, 2^8 - 3 ]

// Head of array of addresses in current book
Block-type=000010=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [metadata-address-type=i16, 2] +
    [next-block-address-type=i16, 2] +
    [address-in-current-book-type=i16, [(2^8-6)/2] ]

// Array of addresses in current book
Block-type=000011=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [next-block-address-type=i16, 2] +
    [address-in-current-book-type=i8, [(2^8-4)/2] ]

// Head of array of addresses in current shelve
Block-type=000100=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [empty-address-type=i24, 3] +
    [next-block-address-type=i24, 3] +
    [address-in-current-shelve-type=i24, [(256 - 8)/3]]

// Array of addresses in current shelve
Block-type=000101=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [next-block-address-type=i24, 3] +
    [address-in-current-shelve-type=i24, [(256 - 5)/3]]

// Head of array of addresses in current sector
Block-type=000110=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [metadata-address-type=i32, 4] +
    [next-block-address-type=i32, 4] +
    [address-in-current-sector-type=i32, [(256 - 10)/4]]

// Array of addresses in current sector
Block-type=000111=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [next-block-address-type=i32, 4] +
    [address-in-current-sector-type=i32, [(256 - 6)/4]]

// Head of array of addresses in current cluster
Block-type=001000=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [metadata-address-type=40, 5] +
    [next-block-address-type=i40, 5] +
    [address-in-current-cluster-type=i40, [(256 - 12)/5]]

// Array of addresses in current cluster
Block-type=001001=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [next-block-address-type=i40, 5] +
    [address-in-current-cluster-type=i40, [(256 - 7)/5]]

// Head of array of addresses in current library
Block-type=001010=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [metadata-address-type=i48, 6] +
    [next-block-address-type=i48, 6] +
    [address-in-current-library-type=i48, [(256 - 14)/6]]

// Array of addresses in current library
Block-type=001011=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [next-block-address-type=i48, 6] +
    [address-in-current-library-type=i48, [(256 - 8)/6]]

// Head of array of addresses in current ethernet
Block-type=001100=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [metadata-address-type=i56, 7] +
    [next-block-address-type=i56, 7] +
    [address-in-current-ethernet-type=i8, [(2^8-16)/7]]

// Array of addresses in current ethernet
Block-type=001101=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [next-block-address-type=i56, 7] +
    [address-in-current-ethernet-type=i56, [(2^8-9)/7]]

// Head of array of addresses in current internet
Block-type=001110=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [metadata-address-type=i64, 8] +
    [next-block-address-type=i64, 8] +
    [address-in-current-internet-type=i64, [(2^8-18)/8]]

// Array of addresses in current page
Block-type=001111=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [next-block-address-type=i64, 8] +
    [address-in-current-internet-type=i64, [(2^8-10)/8]]


// Head of primitively-untyped data with metadata in page
Block-type=010000=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [metadata-address-type=i8, 1] +
    [repetition-of-primitively-untyped-blocks-in-current-page-type=i8, 1 byte] +
    [next-head-of-primitively-untyped-block-type=i8, 1 byte] +
    [block-data-value-type, 2^8 - 5 byte]

//  Head of primitively-untyped data in page
Block-type=010001=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [repetition-of-primitively-untyped-blocks-in-current-page-type=i8, 1 byte] +
    [next-block-after-repetition-type=i8, 1 byte] +
    [block-data-value-type, 2^8 - 4 byte]

// Head of primitively-untyped data with metadata in book
Block-type=010010=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [metadata-address-type=i16, 2] +
    [repetition-of-primitively-untyped-blocks-in-current-book-type=i16, 2 byte] +
    [next-head-of-primitively-untyped-block-type=i16, 2 byte] +
    [block-data-value-type, 2^8 - 8 byte]

//  Head of primitively-untyped data in book
Block-type=010011=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [repetition-of-primitively-untyped-blocks-in-current-book-type=i16, 2 byte] +
    [next-block-after-repetition-type=i16, 2 byte] +
    [block-data-value-type, 2^8 - 6 byte]

// Head of primitively-untyped data with metadata in shelve
Block-type=010100=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [metadata-address-type=i24, 3] +
    [repetition-of-primitively-untyped-blocks-in-current-shelve-type=i24, 3 byte] +
    [next-head-of-primitively-untyped-block-type=i24, 3 byte] +
    [block-data-value-type, 2^8 - 11 byte]

//  Head of primitively-untyped data in shelve
Block-type=010101=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [repetition-of-primitively-untyped-blocks-in-current-shelve-type=i24, 3 byte] +
    [next-block-after-repetition-type=i24, 3 byte] +
    [block-data-value-type, 2^8 - 8 byte]

// Head of primitively-untyped data with metadata in sector
Block-type=010110=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [metadata-address-type=i32, 4] +
    [repetition-of-primitively-untyped-blocks-in-current-sector-type=i32, 4 byte] +
    [next-head-of-primitively-untyped-block-type=i32, 4 byte] +
    [block-data-value-type, 2^8 - 14 byte]

//  Head of primitively-untyped data in sector
Block-type=010111=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [repetition-of-primitively-untyped-blocks-in-current-sector-type=i32, 4 byte] +
    [next-block-after-repetition-type=i32, 4 byte] +
    [block-data-value-type, 2^8 - 10 byte]

// Head of primitively-untyped data with metadata in cluster
Block-type=011000=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [metadata-address-type=i40, 5] +
    [repetition-of-primitively-untyped-blocks-in-current-cluster-type=i40, 5 byte] +
    [next-head-of-primitively-untyped-block-type=i40, 5 byte] +
    [block-data-value-type, 2^8 - 17 byte]

//  Head of primitively-untyped data in cluster
Block-type=011001=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [repetition-of-primitively-untyped-blocks-in-current-cluster-type=i40, 5 byte] +
    [next-block-after-repetition-type=i40, 5 byte] +
    [block-data-value-type, 2^8 - 12 byte]

// Head of primitively-untyped data with metadata in library
Block-type=011010=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [metadata-address-type=i48, 6] +
    [repetition-of-primitively-untyped-blocks-in-current-library-type=i48, 6 byte] +
    [next-head-of-primitively-untyped-block-type=i48, 6 byte] +
    [block-data-value-type, 2^8 - 20 byte]

//  Head of primitively-untyped data in library
Block-type=011011=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [repetition-of-primitively-untyped-blocks-in-current-library-type=i48, 6 byte] +
    [next-block-after-repetition-type=i48, 6 byte] +
    [block-data-value-type, 2^8 - 14 byte]

// Head of primitively-untyped data with metadata in ethernet
Block-type=011100=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [metadata-address-type=i56, 7] +
    [repetition-of-primitively-untyped-blocks-in-current-ethernet-type=i56, 7 byte] +
    [next-head-of-primitively-untyped-block-type=i56, 7 byte] +
    [block-data-value-type, 2^8 - 23 byte]

//  Head of primitively-untyped data in ethernet
Block-type=011101=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [repetition-of-primitively-untyped-blocks-in-current-ethernet-type=i56, 7 byte] +
    [next-block-after-repetition-type=i56, 7 byte] +
    [block-data-value-type, 2^8 - 16 byte]

// Head of primitively-untyped data with metadata in internet
Block-type=011110=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [metadata-address-type=i64, 8] +
    [repetition-of-primitively-untyped-blocks-in-current-internet-type=i64, 8 byte] +
    [next-head-of-primitively-untyped-block-type=i64, 8 byte] +
    [block-data-value-type, 2^8 - 26 byte]

//  Head of primitively-untyped data in internet
Block-type=011111=>[byte-type, 2^8 byte] = 
    [block-type, 6 bit] + 
    [hash-of-this-block-except-this-byte-type=i10, 1 byte + 2 bit] + 
    [repetition-of-primitively-untyped-blocks-in-current-internet-type=i64, 8 byte] +
    [next-block-after-repetition-type=i64, 8 byte] +
    [block-data-value-type, 2^8 - 18 byte]

// metadata of an array of addresses
    [block-data-value-type, 2^8 - x byte]=
         [total-items-of-array-type=i64, 8 byte] +
         [approximate-total-size-on-disk-of-array-type=i64, 8 byte] +
         [permission-group-type=i64, 8 byte] +
         [create-time-type=i48, 6] +
         [access-time-type=i48, 6] +
         [modify-time-type=i48, 6] +
         [empty-addresses-type, x - 42]


// metadata of a primitively-untyped data
    [block-data-value-type, 2^8 - x byte]=
         [approximate-total-size-on-disk-of-array-type=i64, 8 byte] +
         [permission-group-type=i64, 8 byte] +
         [create-time-type=i48, 6] +
         [access-time-type=i48, 6] +
         [modify-time-type=i48, 6] +
         [hash-of-data-type, x - 34]


```

I'm sure these ideas are not new and I have had my own research to come up with the combination of them. However, in the version 4 above, I assumed 

- any metadata for any feature like resilient, snapshots, copy-on-write, etc. are independent of the arrangement of the data on the disk and should be handled on another layer. 
- the most efficient and scalable way is to have a fractal structure to make the addresses as short as possible.
- keep the data in a combination of arrays and linked lists is efficient.
- empty space is a file that has volatility properties to move partially from one directory to another.


It's a draft so I'll make it more clear in the future.

