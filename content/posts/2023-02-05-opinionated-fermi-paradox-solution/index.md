+++
title = "Opinionated Fermi Paradox Solution"
date = 2023-02-05
author = "Hadi Lashkari Ghouchani"
summary = "Efficiency is what we need."
draft = false

[taxonomies]
categories = ["Story", "Imagination", "Fermi Paradox", "Fine-tuning"]
tags = [
 "Fermi Paradox",
 "Fine-tuning",
 "Aliens",
 "Pyramid",
 "Great Filter"
]
+++

![Opinionated Fermi Paradox Solution](wallpaper.png)

Where is everybody? _Enrico Fermi_[^enrico-fermi] asked this question in the summer of 1950 with fellow physicists and the _Fermi Paradox_[^fermi-paradox] born. There are immense number of answers to that question but personally I'm not convinced by them. But I cannot stop myself to think about big questions, so here is my answer.

<!-- more -->
-------------

Above wallpaper reference[^wallpaper]

The idea is that based on all scientific discoveries there's nothing special about human, life, and Earth that cannot be happened on other places. Therefore, similar phenomena, which we call it _extraterrestrial life_[^e-life], must be observable, and even more than that, we should have observed them by now. But we didn't! This is the _Fermi Paradox_[^fermi-paradox]. Even that people are continiously talking about these topics, we already observed all kinds of the signature of all kind of molecules of life all over the place, for instance check out _Discovery in space of ethanolamine, the simplest phospholipid head group_[^phospholipids] paper, but not everyone is convinced. I'm not one of them. It implies that we already observed the life itself, but we just didn't observe _extraterrestrial intellegent life_ or _extraterrestrial civilization_, and that's the whole point of the main question, "where is everybody?". Thus, I justify the meaning of the _Fermi Paradox_ before answering it, because, it looks like the question was related to the intelligent life in the first place, not generally the life itself.

One of the good answers to the _Fermi Pradox_[^fermi-paradox] is the _Great Filter_[^great-filter] where it suggests the existance of the so called _Great Filter_ that eliminates an intelligent life with a super high efficiency! Wikipedia[^fermi-paradox] completed it better than me:

> This probability threshold, which could lie in the past or following human extinction, might work as a barrier to the evolution of intelligent life, or as a high probability of self-destruction.

The _Great Filter_ looks a very promising explanation, but it doesn't specify what is that filter, so we can investigate more about it. That's being said, here we want to consistently specify that piece of the puzzle, therefore, my answer is a sub-theory of the _Great Filter Theory_. We can find our piece just by looking at the question and the _Second law of thermodynamics_[^second-l-t] to come to the conclusion that the intelligent lifes out there are all silence. In other words, they increase the entropy of their invironment less than the threshold that we can observe with our current technology, which can be summarized in **The great filter is the efficiency**. The extraterrestrial civilizations who couldn't be efficient enough got extinct all together, so we cannot find a sign of them. Because efficiency is a percentage, and we know the threshold energy we can currently observe in the universe, then the efficinecy of a type III civilization in the _Kardashev scale_[^k-scale] is calculatable and it's astonishingly a high percentage efficiency, but probably that level of efficiency is what we should get if we want to avoid human extinction.

By here, I'm done with my answer by following restricted logics, so I can post it, but let's ramble around with open imagination!

So, humanity inefficiently shouted out their radio signals, that supposed to be received only on Earth, to the deep space for a century. Also currently we're changing the Earth's climate, which is a sign that how much we're inefficient. Not to mention that we gave the most powerful weapens we ever invented to some of the most stupid people we ever trained, I'm looking at you Mr. Putin! It's strangely odd that we already passed yesterday! I can continue by complaining about the citizenship process of my family in Canada, and how much time it'll take for me to go from work permit visa to that point, and how much inefficient it's, because it's about the lack of reliable and efficient tools, not people's fault of course, but coming from a dictatorship country, Iran, stops me to think loud about it more! Anyway!

Assume Kardashev would define that type IV civilization in a way that they can manage the energy level of the obsrevable universe. Also keep in mind that based on _Superconductors' puzzle pieces_[^superconductor] the speed of light limit is much higher in the extra dimensions, if there are a type IV civilization, they would jump to the higher dimension to travel from one galaxy to the other one. Yes! Like the star wars, but one level up. Nevertheless, such a jump would emit a lot of energy, however, innevitably the most efficient way to pass that amount of energy to the environment would be narrowed down/low frequency waves. As I'm running by imagination, I'm allowed to say maybe the _fast radio burst_[^frb], which currently are mysteries to us, are the signature of such jumps in a terminal of transpostations of a type IV civilization.

If being silence is the next level a civilization could get, couldn't other civilization on Earth already got to that point, but they are silence, so we cannot observe them? I watched _Black Panther: Wakanda Forever_[^wakanda] last night, so it's ordinary to think about such a civilization now. Even that Marvel movies are logically inconsistent, but there are some good ideas, and of course action, there.

But seriously! Imagine we would be an efficient civilization, then we would be the gardener of our environment, but maybe with more distance to be more silence. In fact there were human civilizations who respected jungles and animals around them. The trad-off of controlling the efficiency by controlling the population, or the waste, etc. is that people could lose their autonomy, and randomness, in such a process, which means dictatorships could arise. In fact, it looks like this is exactly what's happened to those old civilizations. A dictatorship is definitly another filter, but because we're talking about advanced civilizations, the possibility that they lose their autonomy by falling into a dictatorship is low, in my humble opinion. That's why I think maybe not all of them had fall into dictatorships. If only one of them could keep people's autonomy they would conclude efficiency is the key, so they would go silence by now.

That's being said, we have to remember _Homo sapiens_[^homo-s] evolved between 200,000 and 100,000 years ago, but before homo sapiens there were other human populations like Neanderthals and Homo erectus, which means they had more time to make a silence civilization. If our careless/inefficient 2000-5000 years old civilization could achieve it in such a short time why not others could do it in a much larger time intervals? But when you review the details, we were oddly supper lucky! Maybe we have gardeners! Anyway!

We're being careless and inefficient lead to a high level of randomness in our _gene pool_[^gene-pool], which generated very clever people like Archimedes, Khayyam, Fermat, Newton, Euler, Gauss, Riemannian, Einstein, etc. Notice civilizations who highly control thier waste, decrease the randomness in their system, which slows down their growth. Also be aware that knowledge is not discoveries, it's inventions, so randomness in the system will lead to different inventions, therefore, our Math, Physics, etc. could be much different from other civilizations, taking into account that abstracted knowledge includes infinity, and you may know that infinity is humongous, and searching such a humongous knowledge tree always needs extra hands. This fact increases the probability of **why being silence and observant is the key. You can study others and learn from them.** In fact, if we carefully look at the history of homo sapiens, we could find a lot of examples that we learned what, and where, to eat and drink from animals in our environment, so this practice is what we already did and it worked.

Before closing this case, I invite you to check _Human evolutionary genetics_ page in the Wikipedia[^human-e-g], or other scientific resources:

> Genetic distance: The genetic difference between humans and chimpanzees is less than 2%, or three times larger than the variation among modern humans (estimated at 0.6%).

This less than 2% genetic distance caused enormous differences between human's _Cognitive skill_[^cognitive-s] and chimpanzees', so another 2% genetic difference could potentially lead to another tribe in the Africa long long time ago, who developed a civilization and go silence before our civilization started. That's a possibility!


This whole thing looks like another conspiracy theory, but taking into account that our observers don't want to decrease the randomness of the system, because that's why they observe us in the first place, because they want to learn about other combinations of facts and its following generated abstractions and knowledge, thus, they don't control us, so it's not a conspiracy theory. We're on our own in this world, and if we get to their efficiency level, we would detect them and they will contact us to make an Earth-origin alliance, but before that happens, expecting to be contacted is meaningless.

In the end, to be more precise, by randomness of the system we just mean the Shannon entropy[^s-entropy] of the system. In fact, entropy is the currency of being silence and observant practices, so the gardeners would estimate how much entropy their garden civilization would lost by just touching it, then only if it worth it they'll touch their garden. That's why I claimed we were oddly lucky in this 2000-5000 years to not going extinct, but other than that they prefer to let us to be on our own. Nevertheless, to just blow your mind about how good these practices are, the IT companies that their managers are more silence and observant, good gardeners, which leads to more autonomy for their employees, are more successfull, even that they may not think in these terms, and even that they don't want to study and learn from their employees. On the other side of the spectrum of these practices you can ask why we were too much lucky in this world from the begining, which is the topic of _Fine-tuning_[^fine-t] arguments. The answer to all of these questions may rely on the entropy as the currency. Too much information/claims in one paragraph, but let me also mention that it's not a god theory, as your manager is not a god, and I have to repeat, so please repeat after me, we're on our own.


-----


# References

[^enrico-fermi]: [Enrico Fermi](https://en.wikipedia.org/wiki/Enrico_Fermi)

[^fermi-paradox]: [Fermi Paradox](https://en.wikipedia.org/wiki/Fermi_paradox)

[^wallpaper]: [Fermi Paradox Solution, alien, wakanda, silence, observant](https://www.midjourney.com/home)

[^e-life]: [Extraterrestrial life](https://en.wikipedia.org/wiki/Extraterrestrial_life)

[^phospholipids]: [Discovery in space of ethanolamine, the simplest phospholipid head group](https://arxiv.org/abs/2105.11141)

[^great-filter]: [Great Filter](https://en.wikipedia.org/wiki/Great_Filter)

[^second-l-t]: [Second law of thermodynamics](https://en.wikipedia.org/wiki/Second_law_of_thermodynamics)

[^k-scale]: [Kardashev scale](https://en.wikipedia.org/wiki/Kardashev_scale)

[^superconductor]: [Superconductors' puzzle pieces](https://hadilq.com/posts/superconductors-puzzle-pieces/)

[^frb]: [Fast radio burst](https://en.wikipedia.org/wiki/Fast_radio_burst)

[^wakanda]: [Black Panther: Wakanda Forever](https://en.wikipedia.org/wiki/Black_Panther:_Wakanda_Forever)

[^homo-s]: [Homo sapiens](https://simple.wikipedia.org/wiki/Homo_sapiens)

[^gene-pool]: [Gene pool](https://en.wikipedia.org/wiki/Gene_pool)

[^human-e-g]: [Human evolutionary genetics](https://en.wikipedia.org/wiki/Human_evolutionary_genetics)

[^cognitive-s]: [Cognitive skill](https://en.wikipedia.org/wiki/Cognitive_skill)

[^s-entropy]: [Entropy (information theory)](https://en.wikipedia.org/wiki/Entropy_(information_theory))

[^fine-t]: [Fine-tuning](https://en.wikipedia.org/wiki/Fine-tuning)

# Cite
If you found this work useful, please consider citing:
```
@misc{hadilq2023Fermi,
    author = {{Hadi Lashkari Ghouchani}},
    note = {Published electronically at \url{https://hadilq.com/posts/opinionated-fermi-paradox-solution/}},
    gitlab = {Gitlab source at \href{https://gitlab.com/hadilq/hadilq.gitlab.io/-/blob/main/content/posts/2023-02-05-opinionated-fermi-paradox-solution/index.md}},
    title = {Opinionated Fermi Paradox Solution},
    year={2023},
}
```

