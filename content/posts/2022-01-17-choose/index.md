+++
title = "Choose"
date = 2022-01-17
author = "Hadi Lashkari Ghouchani"
summary = ""
draft = false

[taxonomies]
categories = ["Lifestyle", "Future"]
tags = [
 "Investment",
 "Donation",
 "Learned Helplessness",
 "Conspiracy Theory",
]
+++

![Don't choose extinction](wallpaper.jpg)

As an observation, I see a lot of people tend to believe on some kind of _Conspiracy Theories_, which for them it means they believe they don't have any _choice_ in some aspects of their lives that naturally they should have. Again as I observed them, they suffer from some kind of _Learned Helplessness_ to reach to their conclusions. This happens to people when they frequently get disappointed while they try to _choose_ in their lives. So this is my guide for myself to how to _choose_ in my life for the long term achievements.

<!-- more -->
-------------

Above wallpaper reference[^wallpaper]

# How to choose?
I don't want to write about the Philosophy of free will here. This post meant to be a simple guide to pursue my long term goals. The future is not predictable. Even if this is all deterministic, the randomness in the _Chaos Theory_[^chaos] will avoid  even very smart people to predict the future, so no group of people control how we live, which means  I don't want to learn helplessness[^learn_help], therefore I have to express, at least to myself, that I choose.

Following is the list that gives me the feeling of _choosing_.

### Elections
They ask you to vote even in nondemocratic countries, so vote, but you may need to be persistent to defend yours and others' right to choose, even after the elections, where mostly people miss that part. I don't want to write more about it, but just mentioned election to have a complete list of how to choose for long term and in what scale I mean by long term goals.

### Investment
You can choose with money[^money_energy] by changing its flow even for a small bit and because of its chaotic nature it can have big impacts. Don't invest for analytically predicted short term profit. Invest on technologies or services that you want your children use them. If you successfully predict the usefulness of a technology or service you probably make a lot of money, but more than that you've chosen them to be available for that profit. Not just the availability, they'll be available _cheaper_ in the future. That's both awesome and terrifying.

With this description it looks a little vague even to me so let's make an example. Look at the prices of current commodities around us, of course in average of brands, reigns, etc. Also the price of a similar amount like 1kg. The sugar price is lower than apple price or meat price is cheaper than vegtables. For sure there are good reasons for these price differences, but if we remove _investigation for cheaper products_ from those reasons, there's nothing else significant enough to decide why it's so. This happens because people in the past predicted the demand for sugar or meat will be higher in the future, so they invested more on those goods. As the result, the supply of those goods increased in a way that their price is cheaper now. The past people chose for us to consume sugar more than apple, and we fall for that, because it's really cheaper now!

In the end, by investigating on goods or services that you want your children use cheaper in the future, you can choose.

### Purchase
It looks like there's a loop in the argument of the above section. The missing piece is the buyers who choose to consume sugar more than apple, so investors invested more on sugar. Thus paying to buy everyday goods and services is a choice that will be permanent in your future. Try to buy goods or services that you want your children consume in the future, not for any reasoning or beliefs they'll have. They'll consume it simply because it'll be cheaper.

In the end, it looks like buying simplest stuff is like an _investment_ on long term goals of the society. This is a lot of responsibility when you want to choose which product you should buy. It's now at least four items that you should consider before buying.

- Lower price, where everyone wants to buy with less money.
- Higher quality, where everyone wants to have a good quality product in their hand.
- Is this product what I want to have access cheaper in the future.
- Is this brand what I want to exist in the future. Where will they spend their money? What are their long term goals?

For sure you cannot solve all these problems when you want to buy any stuff. You need to develop a strategy, especially to have answers to those two last questions always in hand. For instance, you know for every product you buy, that produced in China, you actually pay to their government. Do you want Chinese government exists in the long term? If we had healthy and thoughtful societies, more democratic governments would sell more products, even if their products are a little bit more expensive.

### Donation
If you think your children deserve to use a service or technology, pay for it even if it's not cheap. Don't worry! They'll use it much cheaper because of what you're paying right now. If it's a free service, it'll exist in the future. I personally donate to Wikipedia, and some online services in monthly basis.

The big catch is, don't ever donate in the street to people you just saw, especially poor people, because by donating them you actually investing on seeing more people like that in the future, where probably is against your intention there.

### Attention
As long as time as a resource is restricted, it's an expensive asset, so don't waste it on stuff that you don't want your children spend their time on it. By spending time on it now, you're making its demand higher, thus you'll make its supply higher so its price will be cheaper for the future generations.

# Conclusion
I can write here a lot of observations which will lead to a lot of stories, that how people choose their nondemocratic governments by let their government grab their attention, or how people choose existance of plastic pollutions in their body, etc. but I want the destination of this post to be on another direction.

### Wealthy people
At the first sight on the main argument of this post, someone can say so the rich people will decide for the future of the society, simply because they invest more and they purchase more. And it's not far from reallity. Just look at the price of the fake jewellery. Jewellery is a good example for how rich people can influence the prices because they are expensive and only rich people can buy them. Fake jewelleries are working as best as real ones to shine and create some kind of beauty, but they just made out of different materials. Did you ever ask yourself why people struggle to try different materials to get the same result and sell it "cheaper"? Because, rich people have choosen for the rest of society to increase the supply of jewellery by increasing their demand, then society was creative to invent the fake ones, so now they are available to even poor people.

Therfore indeed rich people are more powerfull to decide for the future of the society. A healthy society would thought more about who deserves to be rich, just by simply donate to people who deserves more, even if there is no other way to push their choices.

Also I have an advise to wealthy people. I always watch you complaining about the taxes, and if you're a fair person and don't complain about why you should pay it at all, you would just complain about why you should pay the government when they clearly waste it. If you want to pay the taxes in a more efficient manner to solve problems of the society, you should start by paying taxes to non-government people. If those people deserve it more and get the power by your donation they can solve the tax problem and other problems as well. Did you notice here we used "tax to non-governemnt people" instead of "donation"? Because it must be in a regular basis, and it must be a fixed percentage of your revenue, where you should be proud of it and promote it for your service or good. Therefore, people who want to choose notice in which hand their purchase money will end up.

### Non-rich people
But these all don't mean middle level people or even poor people don't have the choices. Again as an observation, these people tend to believe on some kind of _Conspiracy Theory_[^cons_theo] more than rich ones because the probability of finding  themselves helpless is higher, which is understandable. My advise to them is that please don't underestimate the power of your attention. You can choose big things with this power, and currently you're choosing anyway! Just watch what you're choosing!

-----

# References

[^wallpaper]: [Don't choose extinction](https://iran.un.org/en/156270-dinosaur-ahead-cop26-dont-choose-extinction)

[^posts]: As a general thought process for my posts in my blog, I decided to write shorter posts, so here we go.

[^cons_theo]: [Conspiracy Theory](https://en.wikipedia.org/wiki/Conspiracy_theory)

[^learn_help]: [Learned Helplessness](https://en.wikipedia.org/wiki/Learned_helplessness)

[^choas]: [Chaos Theory](https://en.wikipedia.org/wiki/Chaos_theory)

[^money_energy]: To make the importance of money in society clear, I think I can claim that money is the theory that humanity is borrowed from a pattern in nature that named _Conservation of energy_[^con_energy]. Don't ask me that money is invented before the discovery of the concept of energy, because I would say the concept of atom was there long before we discover atoms.

[^con_energy]: [Conservation of Energy](https://en.wikipedia.org/wiki/Conservation_of_energy)

