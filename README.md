
# Hadi Lashkari Ghouchani's notes

This is the source of my blog. It's powerd by [Zola](https://www.getzola.org/) and you can find it in [hadilq.com](https://hadilq.com).

This repository is depending on _DeepThought_  as a git submodule so if you want to clone it you should do
```
git clone --recursive git@gitlab.com:hadilq/hadilq.gitlab.io.git
```
or if you already cloned it you should run
```
git submodule update --init --recursive
```

