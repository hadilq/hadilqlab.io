{ nixpkgs ? import <nixpkgs> { } }:
with nixpkgs.pkgs;
pkgs.mkShell {
  name = "zola";
  buildInputs = [
    zola
  ];
}

